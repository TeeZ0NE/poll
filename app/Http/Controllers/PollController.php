<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Region;
use Illuminate\View\View;
use Illuminate\Support\Facades\{Cache, DB};

/**
 * Class PollController
 * @package App\Http\Controllers
 */
class PollController extends Controller
{
    /**
     * Get all regions with parents and return view
     *
     * @param integer $id_query query ID goes outside from
     *
     * @return View
     */
    public function index(int $id_query)
    {
        $regions = Cache::remember('regions', env('CACHE_TTL', 108000), function () {
            return Region::with(['region2', 'region1'])
                ->get();
        });
        return view('pages.index')
            ->with(['regions' => $regions, 'id_query' => $id_query]);
    }

    /**
     * Store 2 database
     *
     * @param int $id_query
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(int $id_query, Request $request)
    {
        foreach ($request->all() as $k => $v) {
            try {
                if ($k == '_token') continue;
                DB::table('op_query_test')->insert(
                    ['id_query' => $id_query,
                        'name' => $k,
                        'result' => is_array($v) ? implode(',', $v) : $v
                    ]);
            } catch (\Exception $err) {

                return redirect()->back()->withErrors(['msg' => $err]);
            }
        }

        return redirect(route('stored', ['id_query' => $id_query]));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function stored()
    {
        return response()->json(['Відповідь' => 'Внесено до бази']);

    }
}
