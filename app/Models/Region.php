<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Region extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'spr_locality';
    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * Get region name in first upper case only.
     *
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return Str::title($value);
    }

    public function region2()
    {
        return $this->belongsTo(Region2::class, 'id_district');
    }

    public function region1()
    {
        return $this->belongsTo(Region1::class, 'id_region');
    }
}
