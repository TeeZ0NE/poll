<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region2 extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'spr_district';
    /**
     * @var bool $timestamps
     */
    public $timestamps = false;
    /**
     * @var array $patterns
     */
    private $patterns = [
        '/РАЙОН/ui', '/СМТ/u', '/М\./u', '/С\./u', '/АВТОНОМНА РЕСПУБЛІКА КРИМ/u', '/ОБЛАСТЬ/ui', '/м.київ/ui', '/м.севастополь/ui',
    ];
    /**
     * @var array $replacements
     */
    private $replacements = [
        'район', 'смт', 'м.', 'с.', 'Автономна Республіка Крим', 'область', 'м.Київ', 'м.Севастополь',
    ];

    /**
     * @param string $value
     * @return string
     */
    public function getNameAttribute(string $value): string
    {
        preg_match('/(?P<name>[\w ]+)(?=\/)/ui', $value, $regionMatch);
        $name = mb_convert_case($regionMatch['name'] ?? $value, MB_CASE_TITLE, 'UTF-8');
        # additional replacements
        return preg_replace($this->patterns, $this->replacements, $name);
    }
}
