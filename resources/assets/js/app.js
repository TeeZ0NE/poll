/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('popper.js');
const datepicker = require('js-datepicker/src/datepicker');
const picker = datepicker('.date-picker', {
    startDay: 1,
    customMonths: ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
    customDays: ['Нд', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    formatter: (input, date, instance) => {
        const value = date.toLocaleDateString()
        input.value = value // => '1/1/2099'
    }
});
$('[data-toggle="tooltip"]').tooltip();
let className, title, clientType;

/**
 * Take client type dialog
 */
$(".fr-1 button").on("click", function (event) {
    title = $(this).text();
    if (className !== undefined || className !== null) {
        $(`.fr-2-earlier [data-dialog=${className}]`).addClass("d-none");
    }
    className = getBtnClassName($(this).attr('class'));
    if (className === 'qC-1-3') {
        clientType = 3;
        showFr3();
    } else {
        $(`.fr-2-earlier [data-dialog=${className}]`).removeClass("d-none");
        $('.fr-1').addClass('d-none');
        $('.title').text(title);
    }
});

/**
 * Get button class
 * The same name will show client type dialog
 */
function getBtnClassName(btn) {
    return btn.match(/(qC-\d-\d)/)[1];
}

/**
 * Back button event
 */
$(".fr-2-earlier .back-btn").on("click", function (event) {
    let fr1 = $(".fr-1");
    $(`.fr-2-earlier [data-dialog=${className}]`).addClass("d-none");
    if (fr1.hasClass('d-none')) fr1.removeClass('d-none');
    clearData();
    event.stopImmediatePropagation();
});

/**
 * set client type
 */
$(".dialog-client-type button").on("click", function (event) {
    clientType = $(this).data('client-type');
    event.stopImmediatePropagation();
    showFr3();
});

/**
 * Design qC-2-2
 * Double question
 */
$(".fr-2-earlier [data-dialog=qC-2-2] button").on("click", function () {
    className = getBtnClassName($(this).attr('class'));
    $('.fr-2-earlier [data-dialog=qC-2-2]').addClass("d-none");
    $(".fr-1 ." + className).click();
});

/**
 * Clear data
 * Clear client type and dialog type (class name)
 */
function clearData() {
    clientType = null;
    className = null;
}

/**
 * detach input
 *
 * detach from left panel 2 right panel
 */
$('.fr-3 input[type=checkbox]').on('change', function () {
    $(this).attr('disabled', true);
    $el = $(this).parent().parent().parent().detach();
    $el.appendTo('.sending-form');
});

/**
 * Building type select
 */
$("#inputGroupBuildingType").on('change', function () {
    let heatType = $("#inputGroupHeatType");
    let floor = $("#inputGroupFloor");
    switch ($(this).val()) {
        case 'Приватний будинок (новобудова)':
        case 'Приватний будинок (ремонт заміна вікон)':
            heatType.val('Автономне').change();
            floor.val('Перший або останній').change();
            break;
        default:
            heatType.val($('#inputGroupHeatType > option:first').val());
            floor.val($('#inputGroupFloor  > option:first').val());
            break;
    }
});
/**
 * Heat type select
 */
$("#inputGroupHeatType").on('change', function () {
    let gas = $("#inputGroupGas");
    let distance = $("#inputGroupDistance");
    switch ($(this).val()) {
        case 'Не буде опалюватись':
            gas.val('Ні - не потрібно').change();
            distance.val('Алюмінієва').change();
            break;
        default:
            gas.val($('#inputGroupGas > option:first').val());
            distance.val($('#inputGroupDistance  > option:first').val());
            break;
    }
});

/**
 * Floor type select
 */
$("#inputGroupFloor").on('change', function () {
    let highSecurity = $("#inputGroupHighSecurity");
    switch ($(this).val()) {
        case 'Вище першого':
            highSecurity.val('Ні').change();
            break;
        default:
            highSecurity.val($('#inputGroupHighSecurity > option:first').val());
            break;
    }
});

/**
 * High security select
 */
$("#inputGroupHighSecurity").on('change', function () {
    let antiMaugli = $("#inputGroupAntiMaugly");
    switch ($(this).val()) {
        case 'Так':
            antiMaugli.val('Так-Замок антимауглі').change();
            // show high jack dialog
            highJack();
            break;
        default:
            antiMaugli.val($('#inputGroupAntiMaugly > option:first').val());
            break;
    }
});
/**
 * Show dialog high Jack again even Yes selected
 */
$("#inputGroupHighSecurity option[value=Так]").on("click", function () {
    highJack();
});
/**
 * Energy saving select
 */
$("#inputGroupEnergysaving").on('change', function () {
    let gas = $("#inputGroupGas");
    switch ($(this).val()) {
        case 'Ні - без енерго':
            gas.val('Ні - не потрібно').change();
            break;
        default:
            gas.val($('#inputGroupGas> option:first').val());
            break;
    }
});

/**
 * Showing fr-3
 *
 * When change client type show fr-3 parent row
 */
function showFr3() {
    // removing parents
    $(".fr-1").remove();
    $(".fr-2-earlier").remove();
    // show main questions
    $(".fr-3").parent().removeClass('d-none');
    // show needed elements
    if ([2, 3, 4, 5, 6, 8, 9, 10, 11].includes(clientType)) {
        $arr = ['datepicker', 'regions', 'buildType', 'heatType', 'creditCompensation', 'profileType', 'windColor', 'energySaving', 'setup', 'windowSill', 'tides', 'mosquitoGrid'];
        $.each($arr, function ($k, $v) {
            removeDNoneFromAttr($v);
        });
    }
    if ([2, 4, 5, 6, 8, 9, 10, 11].includes(clientType)) {
        $arr = ['floor', 'highSecurity', 'antiMaugly', 'sunnySide', 'gas', 'distance'];
        $.each($arr, function ($k, $v) {
            removeDNoneFromAttr($v);
        });
    }
    if ([2, 4, 5, 8, 9, 10, 11].includes(clientType)) {
        removeDNoneFromAttr('windowSillSetup');
    }
}

/**
 * Remove class d-none from date attribute
 */
function removeDNoneFromAttr(attr) {
    $('.fr-3').find(`[data-question=${attr}]`).removeClass('d-none');
}

/**
 * Display additional security level dialog
 */
function highJack() {
    $('#highJackModalCenter').modal('toggle');
}

/**
 * Type (points) of security
 */
$('#highJackModalCenter .modal-body [type=radio]').on('click', function(){
    let intensive = parseInt($("input[name='intensive']:checked").val(),10);
    let help_arrive = parseInt($("input[name='help_arrive']:checked").val(),10);
    let visibilities = parseInt($("input[name='visibilities']:checked").val(),10);
    let canJack = parseInt($("input[name='canJack']:checked").val(),10);
    let sum = intensive + help_arrive + visibilities + canJack;
    $('#highJackResult > span').text(sum);
});
