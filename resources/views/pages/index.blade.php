@extends('layout')
@section('title','Poll')
@section('main_content')
    <div class="container">
        <div class="row bg-light">
            <div class="col-md-12 my-md-2 my-lg-3">
                @include('parts.clientName')
            </div>
        </div>
        @include('parts.firstQuestion')
        @include('parts.secondQuestionEarlier')
        @include('parts.thirdQuestions')
    </div>
@endsection
