<div class="fr-1">
    <div class="row mt-2">
        <div class="col-md-6">
            <button type="button" class="btn btn-primary qC-1-1 h-100 w-100">Порахуйте вікно<br>
                <small> (двері / балкон ...)</small>
            </button>
        </div>
        <div class="col-md-6">
            <button type="button" class="btn btn-secondary qC-2-1 h-100 w-100">Потрібна консультація по товарам
            </button>
        </div>
    </div>
    <div class="row my-3">
        <div class="col-md-6">
            <button type="button" class="btn btn-primary qC-1-2 h-100 w-100">Порахуйте вікно ось такої
                конфігурації...<br>
                <small> (двері / балкон ...)</small>
            </button>
        </div>
        <div class="col-md-6">
            <button type="button" class="btn btn-secondary qC-2-2 h-100 w-100">Цікавлять вікна<br>
                <small> (двері / балкон, відкоси, підвіконня, відливи, москітні сітки...)</small>
            </button>
        </div>
    </div>
    <div class="row my-3">
        <div class="col-md-6 offset-3">
            <button type="button" class="btn btn-primary qC-1-3 h-100 w-100 py-3">Потрібні вікна в підїзд,
                сарай, прості, дешеві<br>
                <small> (двері / балкон ...)</small>
            </button>
        </div>
    </div>
</div>

@include('parts.frames.highJackFrame')
