<div class="modal fade" id="highJackModalCenter" tabindex="-1" role="dialog"
     aria-labelledby="highJackModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Визначення необхідного класу зламонебезпеки</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row py-2">
                        <div class="col-6">
                            <div class="card">
                                <div class="card-header">
                                    Положення будівлі
                                </div>
                                <div class="card-body">
                                    <div class="form-check">
                                        <input class="form-check-input" name="intensive" type="radio"
                                               id="intensiveRadio1"
                                               value="10" checked>
                                        <label class="form-check-label" for="intensiveRadio1">
                                            Вулиці з інтенсивним рухом
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" name="intensive" type="radio"
                                               id="intensiveRadio2"
                                               value="20">
                                        <label class="form-check-label" for="intensiveRadio2">
                                            Вулиці з малоінтенсивним рухом
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" name="intensive" type="radio"
                                               id="intensiveRadio3"
                                               value="30">
                                        <label class="form-check-label" for="intensiveRadio3">
                                            Віддалене місце розташування
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="card">
                                <div class="card-header">
                                    В який час після надходження сигналу можливе прибуття допомоги?
                                </div>
                                <div class="card-body">
                                    <div class="form-check">
                                        <input class="form-check-input" name="help_arrive" type="radio"
                                               id="helpArriveRadio1"
                                               value="10" checked>
                                        <label class="form-check-label" for="helpArriveRadio1">
                                            Протягом 2 хвилин
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" name="help_arrive" type="radio"
                                               id="helpArriveRadio2"
                                               value="20">
                                        <label class="form-check-label" for="helpArriveRadio2">
                                            Протягом 5 хвилин
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" name="help_arrive" type="radio"
                                               id="helpArriveRadio3"
                                               value="30">
                                        <label class="form-check-label" for="helpArriveRadio3">
                                            Протягом 10 хвилин
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-6">
                            <div class="card">
                                <div class="card-header">
                                    Наскільки видимий об'єкт для перехожих?
                                </div>
                                <div class="card-body">
                                    <div class="form-check">
                                        <input class="form-check-input" name="visibilities" type="radio"
                                               id="visibilitiesRadio1"
                                               value="10" checked>
                                        <label class="form-check-label" for="visibilitiesRadio1">
                                            Добре видимий
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" name="visibilities" type="radio"
                                               id="visibilitiesRadio2"
                                               value="20">
                                        <label class="form-check-label" for="visibilitiesRadio2">
                                            Обмежено видимий
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" name="visibilities" type="radio"
                                               id="visibilitiesRadio3"
                                               value="30">
                                        <label class="form-check-label" for="visibilitiesRadio3">
                                            Погано видимий
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="card">
                                <div class="card-header">
                                    Наскільки легким може бути доступ злочинця до об'єкту?
                                </div>
                                <div class="card-body">
                                    <div class="form-check">
                                        <input class="form-check-input" name="canJack" type="radio" id="canJackRadio1"
                                               value="50">
                                        <label class="form-check-label" for="canJackRadio1">
                                            Доволі легкий
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" name="canJack" type="radio" id="canJackRadio2"
                                               value="40">
                                        <label class="form-check-label" for="canJackRadio2">
                                            З невеликим зусиллям
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" name="canJack" type="radio" id="canJackRadio3"
                                               value="20" checked>
                                        <label class="form-check-label" for="canJackRadio3">
                                            Зі значним зусиллям
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-12">
                            <p class="lead alert alert-success" id="highJackResult">Бали:&nbsp;<span>50</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
            </div>
        </div>
    </div>
</div>
