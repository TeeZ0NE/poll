<div class="row d-none dialog-client-type" data-dialog="qC-1-1">
    @include('parts.nav')
    <div class="col">
        <div class="card">
            <div class="card-header title"></div>
            <div class="card-body">
                @include('parts.orderEarlier')
                <div class="row">
                    <div class="col-6 d-flex justify-content-center">
                        <p class="alert alert-danger w-75 h-100 h5 text-center">Вас цікавить якийсь конкретний
                            варіант конфігурації?</p>
                    </div>
                    <div class="col-6 d-flex justify-content-center">
                        <p class="alert alert-success w-75 h-100 h5 text-center">В якій конфігурації ви раніше
                            замовляли вікна?</p>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-6 d-flex justify-content-around">
                        <button type="button" class="btn btn-danger m-1 h-50" data-client-type="2">Я ще не визначився<br>можливо щось
                            порадите?
                        </button>
                        <button type="button" class="btn btn-danger m-1 h-50" data-client-type="4">Так. Порахуйте мені<br>середній
                            варіант по ціні.
                        </button>
                        <button type="button" class="btn btn-danger m-1 h-50" data-client-type="5">Так. Порахуйте *камерний<br>профіль
                            та *склопакет
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-6 d-flex align-content-stretch">
                                <p class="alert alert-success text-center w-100">Не памятаю</p>
                            </div>
                            <div class="col-6 d-flex align-content-stretch"><p
                                    class="alert alert-success text-center">Клієнт знає які вікна
                                    в нього стоять</p></div>
                        </div>
                        <div class="row">
                            <div class="col-6 d-flex align-content-stretch">
                                <p class="alert alert-success text-center">Зараз вас цікавить конретний
                                    варіант конфігурації?</p>
                            </div>
                            <div class="col-6 d-flex align-content-stretch"><p
                                    class="alert alert-success text-center">Ви хочете порахувати такі
                                    самі вікна? Чи якусь іншу конфігурацію?</p></div>
                        </div>
                        <div class="row">
                            <div class="col-3 px-0 d-flex align-content-stretch">
                                <button type="button" class="btn btn-success m-1" data-client-type="10">Так. *камерний профіль *склопакет
                                </button>
                            </div>
                            <div class="col-3 px-0 d-flex align-content-stretch">
                                <button type="button" class="btn btn-success m-1" data-client-type="11">Ні. Я ще не визначився</button>
                            </div>
                            <div class="col-3 px-0 d-flex align-content-stretch">
                                <button type="button" class="btn btn-success m-1" data-client-type="8">Такі самі вікна, мене все
                                    влаштовує.
                                </button>
                            </div>
                            <div class="col-3 px-0 d-flex align-content-stretch">
                                <button type="button" class="btn btn-success m-1" data-client-type="9">Іншу конфігурацію. Можливо щось
                                    порадите?
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
