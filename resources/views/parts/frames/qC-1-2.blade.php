<div class="row d-none dialog-client-type" data-dialog="qC-1-2">
    @include('parts.nav')
    <div class="col">
        <div class="card">
            <div class="card-header title"></div>
            <div class="card-body">
                <p class="card-text lead text-center">Ви вже в нас щось замовляли раніше?</p>
                <div class="row mb-2">
                    <div class="col-6">
                        <button type="button" class="btn btn-danger m-1 w-100 h-25" data-client-type="5">
                            Ні
                        </button>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <p class="col-12 alert alert-success w-100 text-center h3">Так</p>
                            <p class="col-12 alert alert-success">Попередній раз ви замовляли таку ж конфігурацію?</p>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-success m-1" data-client-type="8">
                                    ні зараз цікавить інший варіант
                                </button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-success m-1" data-client-type="10">
                                    я не памятаю що ставив раніше
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
