<div class="row d-none dialog-client-type" data-dialog="qC-2-1">
    @include('parts.nav')
    <div class="col">
        <div class="card">
            <div class="card-header title"></div>
            <div class="card-body">
                @include('parts.orderEarlier')
                <div class="row">
                    <div class="col-6 d-flex justify-content-center">
                        <p class="alert alert-danger w-75 h-100 h5 text-center">Ви хочете проконсультуватись та
                            прицінитись?</p>
                    </div>
                    <div class="col-6 d-flex justify-content-center">
                        <p class="alert alert-success w-75 h-100 h5 text-center">В якій конфігурації ви раніше
                            замовляли вікна?</p>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-6 d-flex justify-content-around">
                        <button type="button" class="btn btn-danger m-1 py-3" data-client-type="6">Ні лише консультація</button>
                        <button type="button" class="btn btn-danger m-1" data-client-type="2">Так і ціна також цікавить
                        </button>
                    </div>
                    <div class="col-6  d-flex justify-content-around">
                        <button type="button" class="btn btn-success m-1 py-3" data-client-type="11">Не знаю</button>
                        <button type="button" class="btn btn-success m-1" data-client-type="9">Клієнт знає які вікна
                            в нього стоять
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
