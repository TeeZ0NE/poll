<div class="row d-none" data-dialog="qC-2-2">
    @include('parts.nav')
    <div class="col">
        <div class="card">
            <div class="card-header title"></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <p class="alert alert-success w-100 h5 text-center">Вас цікавить якийсь конкретний
                            варіант конфігурації?</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <button type="button" class="btn btn-danger m-1 w-100 py-3 qC-1-1">Прицінитись</button>
                    </div>
                    <div class="col-6">
                        <button type="button" class="btn btn-success m-1 w-100 py-3 qC-2-1">Проконсультуватись</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
