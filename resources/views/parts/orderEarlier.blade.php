<p class="card-text lead text-center">Ви вже в нас щось замовляли раніше?</p>
<div class="row mb-2">
    <div class="col-6">
        <p class="alert alert-danger w-100 text-center h3">Ні</p>
    </div>
    <div class="col-6">
        <p class="alert alert-success w-100 text-center h3">Так</p>
    </div>
</div>
