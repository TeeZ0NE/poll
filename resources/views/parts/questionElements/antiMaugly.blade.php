<div class="input-group mb-3 d-none" data-question="antiMaugly">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupAntiMaugly" data-toggle="tooltip" data-placement="bottom"
               title="Я можу запропонувати спеціальну опцію фурнітури яка не дасть змогу дитині самостійно відкрити вікно.">У
            Вас є маленькі діти? (*замок
            антимауглі)</label>
    </div>
    <select class="custom-select" id="inputGroupAntiMaugly" name="анті_мауглі">
        <option selected>Вибрати...</option>
        <option value="Відповідь отримано">Відповідь отримано</option>
        <option value="Ні">Ні</option>
        <option value="Так-Замок антимауглі">Так-Замок антимауглі</option>
        <option value="Так-Ручка з ключем">Так-Ручка з ключем</option>
    </select>
</div>
