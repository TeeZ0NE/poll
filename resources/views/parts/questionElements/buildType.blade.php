<div class="input-group mb-3 d-none" data-question="buildType">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupBuildingType" data-toggle="tooltip" data-placement="bottom"
               title="Потрібно вказати де проходить заміна вікон для того, щоб запропонувати оптимальний варіант конфігурації.">Тип
            приміщення?</label>
    </div>
    <select class="custom-select" id="inputGroupBuildingType" name="тип_будинку">
        <option selected>Вибрати...</option>
        <option value="Приватний будинок (новобудова)">Приватний будинок (новобудова)</option>
        <option value="Приватний будинок (ремонт заміна вікон)">Приватний будинок (ремонт заміна вікон)</option>
        <option value="Квартира">Квартира</option>
        <option value="Комерційне">Комерційне</option>
    </select>
</div>
