<div class="input-group mb-3 d-none" data-question="creditCompensation">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupCreditCompensation" data-toggle="tooltip" data-placement="bottom"
               title="Вас цікавить кредитна програма з отриманням компенсації?">Кредит компенсація
            цікавить?</label>
    </div>
    <select class="custom-select" id="inputGroupCreditCompensation" name="кредит_компенсація">
        <option selected>Вибрати...</option>
        <option value="Відповідь отримано">Відповідь отримано</option>
        <option value="Не цікаво">Не цікаво</option>
        <option value="Цікавить IQ Energy">Цікавить IQ Energy</option>
        <option value="Цікавить держ. компенсація">Цікавить держ. компенсація</option>
    </select>
</div>
