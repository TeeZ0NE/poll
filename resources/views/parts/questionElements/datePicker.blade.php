<div class="input-group mb-3 d-none" data-question="datepicker">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text"
               id="inputGroup-sizing-default" data-toggle="tooltip" data-placement="bottom"
               title="Отримавши інформацію про орієнтовну дату замовлення ми будемо знати чи підпадає замовлення під діючу акцію, чи встигнемо ми виготовити замовлення в строк і т.д.">Коли
            ви плануєте встановлювати вікна?</label>
    </div>
    <input type="text" class="form-control date-picker" aria-label="DataPicker input"
           aria-describedby="inputGroup-sizing-default" name="дата_встановлення">
</div>
