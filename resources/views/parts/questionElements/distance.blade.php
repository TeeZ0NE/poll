<div class="input-group mb-3 d-none" data-question="distance">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupDistance" data-toggle="tooltip" data-placement="bottom" title="Пластикова дистанційна рамка навідміну від алюмінієвої має низький показник теплопровідності, склопакет з пластиковою дистанцією тепліший в середньому на 10%.
Також пластикова дистанція знижує ризик появи конденсату по кромці склопакету.">З якою дистанцією будемо рахувати
            склопакет?</label>
    </div>
    <select class="custom-select" id="inputGroupDistance" name="відстань">
        <option selected>Вибрати...</option>
        <option value="Відповідь отримано">Відповідь отримано</option>
        <option value="Алюмінієва">Алюмінієва</option>
        <option value="Пластикова">Пластикова</option>
    </select>
</div>
