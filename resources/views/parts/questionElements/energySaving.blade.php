<div class="input-group mb-3 d-none" data-question="energySaving">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupEnergysaving" data-toggle="tooltip" data-placement="bottom"
               title="Енергозберігаюче скло - скло з напиленням іонів срібла, яке працює за принципом теплового дзеркала. Воно значно покращує теплоізоляцію склопакета і при цьому коштує не дорого.">Енергозберігаюче
            скло будемо
            рахувати?</label>
    </div>
    <select class="custom-select" id="inputGroupEnergysaving" name="енерго_зберігаюче_скло">
        <option selected>Вибрати...</option>
        <option value="Відповідь отримано">Відповідь отримано</option>
        <option value="Так - 1 лише з середини">Так - 1 лише з середини</option>
        <option value="Так - 2 з середини і з зовні">Так - 2 з середини і з зовні</option>
        <option value="Ні - без енерго">Ні - без енерго</option>
    </select>
</div>
