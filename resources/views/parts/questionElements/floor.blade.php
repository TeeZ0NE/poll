<div class="input-group mb-3 d-none" data-question="floor">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupFloor" data-toggle="tooltip" data-placement="bottom"
               title="Дізнавшись поверх на якому будуть встановлені вікна ми зможемо запропонувати необхідні опції фурнітури, а також врахувати реальні вітрові навантаження.">На
            якому поверсі потрібно замінити
            вікна?</label>
    </div>
    <select class="custom-select" id="inputGroupFloor" name="поверх">
        <option selected>Вибрати...</option>
        <option value="Перший або останній">Перший або останній</option>
        <option value="Вище першого">Вище першого</option>
    </select>
</div>
