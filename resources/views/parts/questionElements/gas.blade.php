<div class="input-group mb-3 d-none" data-question="gas">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupGas" data-toggle="tooltip" data-placement="bottom" title="Для покращення теплоізоляції в камери склопакету можна закачати інертний газ:
- Аргон покращує теплоізоляцію склопакета на 9%
- Криптон поеращує теплоізоляцію склопакета на 17%">Заповнювати склопакет інертним
            газом?</label>
    </div>
    <select class="custom-select" id="inputGroupGas" name="газ">
        <option selected>Вибрати...</option>
        <option value="Відповідь отримано">Відповідь отримано</option>
        <option value="Ні - не потрібно">Ні - не потрібно</option>
        <option value="Так - Аргоном">Так - Аргоном</option>
        <option value="Так - Криптоном">Так - Криптоном</option>
    </select>
</div>
