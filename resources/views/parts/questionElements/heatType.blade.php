<div class="input-group mb-3 d-none" data-question="heatType">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupHeatType" data-toggle="tooltip" data-placement="bottom"
               title="Потрібно вказати тип опалення для того щоб підібрати оптимальний варіант по співвідношенню ціна/енергоефективність">Вид
            опалення?</label>
    </div>
    <select class="custom-select" id="inputGroupHeatType" name="тип_опалення">
        <option selected>Вибрати...</option>
        <option value="Автономне">Автономне</option>
        <option value="Централізоване">Централізоване</option>
        <option value="Не буде опалюватись">Не буде опалюватись</option>
    </select>
</div>
