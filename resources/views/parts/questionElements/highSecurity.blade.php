<div class="input-group mb-3 d-none" data-question="highSecurity">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupHighSecurity" data-toggle="tooltip" data-placement="bottom"
               title="Ми можемо запропонувати спеціальну фурнітуру яка ускладнить доступ до вашої оселі.">Підвищена
            зламобезпека
            цікавить?</label>
    </div>
    <select class="custom-select" id="inputGroupHighSecurity" name="підвищена_зламобезпека">
        <option selected>Вибрати...</option>
        <option value="Ні">Ні</option>
        <option value="Так">Так</option>
    </select>
</div>
