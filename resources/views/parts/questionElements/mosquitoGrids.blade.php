<div class="input-group mb-3 d-none" data-question="mosquitoGrid">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupMosquitoGrids">Москітні сітки порахуємо?</label>
    </div>
    <select class="custom-select" id="inputGroupMosquitoGrids" name="москітні_сітки">
        <option selected>Вибрати...</option>
        <option value="Відповідь отримано">Відповідь отримано</option>
        <option value="Так">Так</option>
        <option value="Ні">Ні</option>
    </select>
</div>
