<div class="input-group mb-3 d-none" data-question="profileType">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <span class="input-group-text" for="inputGroupProfileType">З якого профілю ви хочете порахувати
            конструкції?</span>
    </div>
    <select class="custom-select" id="inputGroupProfileType" name="тип_профілю[]" multiple size="9">
        <option value="Відповідь отримано">Відповідь отримано</option>
        <option value="Ecosol 60">Ecosol 60</option>
        <option value="Ecosol 70">Ecosol 70</option>
        <option value="Korsa Design 70">Korsa Design 70</option>
        <option value="Brillant Design">Brillant Design</option>
        <option value="Sinego AD">Sinego AD</option>
        <option value="Sinego MD">Sinego MD</option>
        <option value="Geneo">Geneo</option>
    </select>
</div>
