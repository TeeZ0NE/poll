<div class="input-group mb-3 d-none" data-question="regions">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupRegion">Населений пункт, куди потрібні вікна?</label>
    </div>
    <input type="text" class="form-control flexdatalist" placeholder="Місто" aria-label="Region"
           aria-describedby="inputGroupRegion" list="regions" name="регіон" data-min-length='1'>
    <datalist id="regions">
                @foreach($regions as $region)
                    @unless($region->name) @continue @endunless
                    <option value="@isset($region->name){{$region->name}},@endisset @if($region->region2->name !== ""){{$region->region2->name}},@endif @if($region->region1->name !== ""){{$region->region1->name}}@endif">@isset($region->name){{$region->name}},@endisset @if($region->region2->name !==""){{$region->region2->name}},@endif @if($region->region1->name !== ""){{$region->region1->name}}@endisset</option>
                @endforeach
    </datalist>
</div>
@push('js-scripts')
    <script>
        /**
         * Options 4 region search filter
         */
        $('.flexdatalist').flexdatalist({
            minLength: 1,
            noResultsText: 'Не знайдено "{keyword}"'
        });
    </script>
@endpush
