<div class="input-group mb-3 d-none" data-question="setup">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupSetup" data-toggle="tooltip" data-placement="bottom" title="При замовлені конструкцій без монтажу гарантія на них не розповсюджується.
Ми можемо виконати звичайний монтаж, але рекомендуємо на вибір ізоляційний монтаж з використанням паро та гідроізолюючих стрічок, який захищає монтажний шов від потрапляння вологи і руйнування та відповідає вимогам ДСТУ.
Що будем рахувати?">Вікна рахувати з монтажем?</label>
    </div>
    <select class="custom-select" id="inputGroupSetup" name="монтаж">
        <option selected>Вибрати...</option>
        <option value="Відповідь отримано">Відповідь отримано</option>
        <option value="Без монтажу">Без монтажу</option>
        <option value="Лише монтаж Звичайний">Лише монтаж Звичайний</option>
        <option value="Лише монтаж Ізоляційний">Лише монтаж Ізоляційний</option>
        <option value="Монтаж/Демонтаж звичайний">Монтаж/Демонтаж звичайний</option>
        <option value="Монтаж/Демонтаж ізоляційний">Монтаж/Демонтаж ізоляційний</option>
        <option value="Виносний монтаж">Виносний монтаж</option>
    </select>
</div>
