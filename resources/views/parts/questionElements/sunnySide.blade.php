<div class="input-group mb-3 d-none" data-question="sunnySide">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupSunnySide" data-toggle="tooltip" data-placement="bottom"
               title="На сонячну сторону рекомендуємо встановити склопакети з мультифункціональним склом Clima Guard Solar завдяки якому сонячні промені значно меньше будуть нагрівати приміщення. Вам рахувати мультифункіональне скло?">Вікна
            виходять на сонячну сторону?</label>
    </div>
    <select class="custom-select" id="inputGroupSunnySide" name="сонячна_сторона">
        <option selected>Вибрати...</option>
        <option value="Відповідь отримано">Відповідь отримано</option>
        <option value="Так">Так</option>
        <option value="Ні">Ні</option>
    </select>
</div>
