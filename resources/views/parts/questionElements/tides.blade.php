<div class="input-group mb-3 d-none" data-question="tides">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupTides">Відливи будемо рахувати?</label>
    </div>
    <select class="custom-select" id="inputGroupTides" name="відливи">
        <option selected>Вибрати...</option>
        <option value="Відповідь отримано">Відповідь отримано</option>
        <option value="Так">Так</option>
        <option value="Ні">Ні</option>
    </select>
</div>
