<div class="input-group mb-3 d-none" data-question="windColor">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <span class="input-group-text" for="inputGroupWindColor" data-toggle="tooltip" data-placement="bottom"
               title="Ми можемо виготовити вікна в білому кольорі, або з односторонньою, двухсторонньою ламінацією. Ламінація може бути однотонна або під структуру дерева. Також можемо пофарбувати профіль під будь-який колір по RAL">В
            якому кольорі повинні бути ваші вікна?</span>
    </div>
    <select class="custom-select" id="inputGroupWindColor" name="колір_вікна[]" multiple size="8">
        <option value="Відповідь отримано">Відповідь отримано</option>
        <option value="в білому">в білому</option>
        <option value="1 ст. Ламінація">1 ст. Ламінація</option>
        <option value="2 ст. Ламінація">2 ст. Ламінація</option>
        <option value="1 ст. Ламінація ексклюзив">1 ст. Ламінація ексклюзив</option>
        <option value="2 ст. Ламінація ексклюзив">2 ст. Ламінація ексклюзив</option>
        <option value="Покраска 1 ст.">Покраска 1 ст.</option>
        <option value="Покраска 2 ст.">Покраска 2 ст.</option>
    </select>
</div>
