<div class="input-group mb-3 d-none" data-question="windowSill">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupWindowsill">Підвіконники будемо рахувати?</label>
    </div>
    <select class="custom-select" id="inputGroupWindowsill" name="підвіконня">
        <option selected>Вибрати...</option>
        <option value="Відповідь отримано">Відповідь отримано</option>
        <option value="Не потрібно">Не потрібно</option>
        <option value="Open Teck">Open Teck</option>
        <option value="Kraft">Kraft</option>
        <option value="Danke">Danke</option>
        <option value="Danke (Стандарт)">Danke (Стандарт)</option>
        <option value="Danke (Комфорт)">Danke (Комфорт)</option>
    </select>
</div>
