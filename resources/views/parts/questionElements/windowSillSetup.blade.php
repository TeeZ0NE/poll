<div class="input-group mb-3 d-none" data-question="windowSillSetup">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Input filled">
        </div>
        <label class="input-group-text" for="inputGroupWindowsillSetup" data-toggle="tooltip" data-placement="bottom"
               title="Система для монтажу за технологією «Тепле підвіконня» ТМ КОРСА являє собою конструкцію з полістирольної плити з монтажною полицею, на яку встановлюється вікно, відлив і підвіконня.">Вікно монтувати на основу теплого підвіконня, чи стіну?</label>
    </div>
    <select class="custom-select" id="inputGroupWindowsillSetup" name="монтаж_підвіконння">
        <option selected>Вибрати...</option>
        <option value="Відповідь отримано">Відповідь отримано</option>
        <option value="Основа теплого підвіконня">Основа теплого підвіконня</option>
        <option value="Не потрібно">Не потрібно</option>
    </select>
</div>
