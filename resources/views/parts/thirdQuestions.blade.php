<div class="row d-none">
    <div class="col-12 col-lg-6 fr-3 py-2">
        @include('parts.questionElements.datePicker')
        @include('parts.questionElements.regions')
        @include('parts.questionElements.buildType')
        @include('parts.questionElements.heatType')
        @include('parts.questionElements.floor')
        @include('parts.questionElements.highSecurity')
        @include('parts.questionElements.antiMaugly')
        @include('parts.questionElements.creditCompensation')
        @include('parts.questionElements.profileType')
        @include('parts.questionElements.windColor')
        @include('parts.questionElements.sunnySide')
        @include('parts.questionElements.energySaving')
        @include('parts.questionElements.gas')
        @include('parts.questionElements.distance')
        @include('parts.questionElements.setup')
        @include('parts.questionElements.windowSill')
        @include('parts.questionElements.windowSillSetup')
        @include('parts.questionElements.tides')
        @include('parts.questionElements.mosquitoGrids')
    </div>
    <div class="col-12 col-lg-6 border border-success py-2">
        <form action="{{route('store-poll',['id_query'=>$id_query])}}" method="POST">{{csrf_field()}}
            <div class="sending-form"></div>
            <div class="text-center">
                <button type="submit" class="btn btn-dark">Відправити результати</button>
            </div>
        </form>
    </div>
</div>
