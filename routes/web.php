<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/{id_query}', 'PollController@index')->name('home');
Route::post('store/{id_query}', 'PollController@store')->name('store-poll');
Route::get('/stored/{id_query}', 'PollController@stored')->name('stored');
