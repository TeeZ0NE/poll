const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
    'resources/assets/js/app.js',
    'resources/assets/js/jquery.flexdatalist.js'
], 'public/js/app.js')
    .stylus('resources/assets/stylus/app.styl', 'public/css')
    .less('node_modules/js-datepicker/src/datepicker.less', 'public/css')
;
mix.autoload({
    jquery: ['$', 'window.jQuery', 'jQuery'],
});
mix.webpackConfig({
    resolve: {
        modules: [
            'node_modules'
        ],
        alias: {
            jquery: 'jquery/src/jquery',
            datepicker: 'js-datepicker/src/datepicker'
        }
    }
});
if (mix.inProduction()) {
    mix.version();
}
